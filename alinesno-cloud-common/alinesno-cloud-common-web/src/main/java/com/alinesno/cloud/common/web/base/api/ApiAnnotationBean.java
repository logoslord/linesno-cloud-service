package com.alinesno.cloud.common.web.base.api;
/**
 * 注解值 
 * @author LuoAnDong
 *
 */
public class ApiAnnotationBean {
	
	private String method ; 
	private boolean validate ; 
	private String desc ;
	
	public String getMethod() {
		return method;
	}
	public void setMethod(String method) {
		this.method = method;
	}
	public boolean isValidate() {
		return validate;
	}
	public void setValidate(boolean validate) {
		this.validate = validate;
	}
	public String getDesc() {
		return desc;
	}
	public void setDesc(String desc) {
		this.desc = desc;
	} 
	
	
}