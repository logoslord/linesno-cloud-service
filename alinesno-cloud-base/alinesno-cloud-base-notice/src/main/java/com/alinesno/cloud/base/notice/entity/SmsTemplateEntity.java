package com.alinesno.cloud.base.notice.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.alinesno.cloud.common.core.orm.entity.BaseEntity;


/**
 * <p>
 * 
 * </p>
 *
 * @author LuoAnDong
 * @since 2018-12-02 16:08:00
 */
@Entity
@Table(name="sms_template")
public class SmsTemplateEntity extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 所属应用
     */
	@Column(name="application_id")
	private String applicationId;
    /**
     * 所属租户
     */
	@Column(name="tenant_id")
	private String tenantId;
    /**
     * 短信模板。
     */
	@Column(name="template_content")
	private String templateContent;
    /**
     * 模板描述
     */
	@Column(name="template_desc")
	private String templateDesc;


	public String getApplicationId() {
		return applicationId;
	}

	public void setApplicationId(String applicationId) {
		this.applicationId = applicationId;
	}

	public String getTenantId() {
		return tenantId;
	}

	public void setTenantId(String tenantId) {
		this.tenantId = tenantId;
	}

	public String getTemplateContent() {
		return templateContent;
	}

	public void setTemplateContent(String templateContent) {
		this.templateContent = templateContent;
	}

	public String getTemplateDesc() {
		return templateDesc;
	}

	public void setTemplateDesc(String templateDesc) {
		this.templateDesc = templateDesc;
	}


	@Override
	public String toString() {
		return "SmsTemplateEntity{" +
			"applicationId=" + applicationId +
			", tenantId=" + tenantId +
			", templateContent=" + templateContent +
			", templateDesc=" + templateDesc +
			"}";
	}
}
