package com.alinesno.cloud.base.notice.rest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.alinesno.cloud.base.notice.entity.SmsSettingsEntity;
import com.alinesno.cloud.base.notice.service.ISmsSettingsService;
import com.alinesno.cloud.common.core.rest.BaseRestController;

/**
 * <p> 接口 </p>
 *
 * @author LuoAnDong
 * @since 2018-12-02 16:08:00
 */
@Scope("prototype")
@RestController
@RequestMapping("smsSettings")
public class SmsSettingsRestController extends BaseRestController<SmsSettingsEntity , ISmsSettingsService> {

	//日志记录
	@SuppressWarnings("unused")
	private final static Logger logger = LoggerFactory.getLogger(SmsSettingsRestController.class);

}
