package com.alinesno.cloud.base.notice.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.alinesno.cloud.common.core.orm.entity.BaseEntity;


/**
 * <p>
 * 
 * </p>
 *
 * @author LuoAnDong
 * @since 2018-12-02 16:08:00
 */
@Entity
@Table(name="email_send")
public class EmailSendEntity extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 接收邮件
     */
	@Column(name="email_receiver")
	private String emailReceiver;
    /**
     * 发送者
     */
	@Column(name="email_sender")
	private String emailSender;
    /**
     * 邮件内容
     */
	@Column(name="email_content")
	private String emailContent;
    /**
     * 发送时间 
     */
	@Column(name="email_send_time")
	private Date emailSendTime;
    /**
     * 发送次数
     */
	@Column(name="email_send_count")
	private Integer emailSendCount;
    /**
     * 邮件标题
     */
	@Column(name="email_subject")
	private String emailSubject;
    /**
     * 邮件类型
     */
	@Column(name="email_type")
	private String emailType;
    /**
     * 所属应用
     */
	@Column(name="application_id")
	private String applicationId;
    /**
     * 所属租户
     */
	@Column(name="tenant_id")
	private String tenantId;

	public String getEmailReceiver() {
		return emailReceiver;
	}

	public void setEmailReceiver(String emailReceiver) {
		this.emailReceiver = emailReceiver;
	}

	public String getEmailSender() {
		return emailSender;
	}

	public void setEmailSender(String emailSender) {
		this.emailSender = emailSender;
	}

	public String getEmailContent() {
		return emailContent;
	}

	public void setEmailContent(String emailContent) {
		this.emailContent = emailContent;
	}

	public Date getEmailSendTime() {
		return emailSendTime;
	}

	public void setEmailSendTime(Date emailSendTime) {
		this.emailSendTime = emailSendTime;
	}

	public Integer getEmailSendCount() {
		return emailSendCount;
	}

	public void setEmailSendCount(Integer emailSendCount) {
		this.emailSendCount = emailSendCount;
	}

	public String getEmailSubject() {
		return emailSubject;
	}

	public void setEmailSubject(String emailSubject) {
		this.emailSubject = emailSubject;
	}

	public String getEmailType() {
		return emailType;
	}

	public void setEmailType(String emailType) {
		this.emailType = emailType;
	}

	public String getApplicationId() {
		return applicationId;
	}

	public void setApplicationId(String applicationId) {
		this.applicationId = applicationId;
	}

	public String getTenantId() {
		return tenantId;
	}

	public void setTenantId(String tenantId) {
		this.tenantId = tenantId;
	}


	@Override
	public String toString() {
		return "EmailSendEntity{" +
			"emailReceiver=" + emailReceiver +
			", emailSender=" + emailSender +
			", emailContent=" + emailContent +
			", emailSendTime=" + emailSendTime +
			", emailSendCount=" + emailSendCount +
			", emailSubject=" + emailSubject +
			", emailType=" + emailType +
			", applicationId=" + applicationId +
			", tenantId=" + tenantId +
			"}";
	}
}
