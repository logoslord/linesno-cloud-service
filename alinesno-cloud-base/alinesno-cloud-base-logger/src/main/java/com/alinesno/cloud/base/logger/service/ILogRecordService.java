package com.alinesno.cloud.base.logger.service;

import org.springframework.data.repository.NoRepositoryBean;

import com.alinesno.cloud.base.logger.entity.LogRecordEntity;
import com.alinesno.cloud.base.logger.repository.LogRecordRepository;
import com.alinesno.cloud.common.core.services.IBaseService;

/**
 * <p>  服务类 </p>
 *
 * @author LuoAnDong
 * @since 2018-12-16 18:16:06
 */
@NoRepositoryBean
public interface ILogRecordService extends IBaseService<LogRecordRepository, LogRecordEntity, String> {

}
