package com.alinesno.cloud.base.logger.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.alinesno.cloud.base.logger.entity.LogTypeEntity;
import com.alinesno.cloud.base.logger.repository.LogTypeRepository;
import com.alinesno.cloud.base.logger.service.ILogTypeService;
import com.alinesno.cloud.common.core.services.impl.IBaseServiceImpl;

/**
 * <p>  服务实现类 </p>
 *
 * @author LuoAnDong
 * @since 2018-12-16 18:16:06
 */
@Service
public class LogTypeServiceImpl extends IBaseServiceImpl<LogTypeRepository, LogTypeEntity, String> implements ILogTypeService {

	//日志记录
	@SuppressWarnings("unused")
	private static final Logger log = LoggerFactory.getLogger(LogTypeServiceImpl.class);

}
