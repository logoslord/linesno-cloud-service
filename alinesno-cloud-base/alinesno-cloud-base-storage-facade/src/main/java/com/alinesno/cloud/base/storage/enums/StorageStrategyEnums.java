package com.alinesno.cloud.base.storage.enums;

/**
 * 文件保存策略
 * @author LuoAnDong
 * @since 2019年4月10日 上午6:58:39
 */
public enum StorageStrategyEnums {

	LOCAL("local") , 
	QINIU("qiniu") , 
	ALIOSS("alioss") ; 

	private String value ; 
	
	StorageStrategyEnums(String value){
		this.value = value ; 
	}
	
	public String value() {
		return value ; 
	}
}
