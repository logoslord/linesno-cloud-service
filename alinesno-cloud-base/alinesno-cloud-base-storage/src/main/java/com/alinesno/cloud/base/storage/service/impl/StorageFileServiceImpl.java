package com.alinesno.cloud.base.storage.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.alinesno.cloud.base.storage.entity.StorageFileEntity;
import com.alinesno.cloud.base.storage.repository.StorageFileRepository;
import com.alinesno.cloud.base.storage.service.IStorageFileService;
import com.alinesno.cloud.common.core.services.impl.IBaseServiceImpl;

/**
 * <p>  服务实现类 </p>
 *
 * @author LuoAnDong
 * @since 2019-04-09 20:22:17
 */
@Service
public class StorageFileServiceImpl extends IBaseServiceImpl<StorageFileRepository, StorageFileEntity, String> implements IStorageFileService {

	//日志记录
	@SuppressWarnings("unused")
	private static final Logger log = LoggerFactory.getLogger(StorageFileServiceImpl.class);

}
