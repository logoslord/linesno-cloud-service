package com.alinesno.cloud.base.boot.service;

import org.springframework.data.repository.NoRepositoryBean;

import com.alinesno.cloud.base.boot.entity.ContentLinksEntity;
import com.alinesno.cloud.base.boot.repository.ContentLinksRepository;
import com.alinesno.cloud.common.core.services.IBaseService;

/**
 * <p>  服务类 </p>
 *
 * @author LuoAnDong
 * @since 2018-12-16 17:53:19
 */
@NoRepositoryBean
public interface IContentLinksService extends IBaseService<ContentLinksRepository, ContentLinksEntity, String> {

}
