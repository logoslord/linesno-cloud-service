package com.alinesno.cloud.base.message.rest;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.alinesno.cloud.base.message.entity.TransactionMessageEntity;
import com.alinesno.cloud.base.message.entity.TransactionMessageHistoryEntity;
import com.alinesno.cloud.base.message.service.ITransactionMessageService;
import com.alinesno.cloud.common.core.rest.BaseRestController;

/**
 * <p>
 * 接口
 * </p>
 *
 * @author LuoAnDong
 * @since 2018-12-16 18:25:27
 */
@Scope("prototype")
@RestController
@RequestMapping("transactionMessage")
public class TransactionMessageRestController
		extends BaseRestController<TransactionMessageEntity, ITransactionMessageService> {

	// 日志记录
	@SuppressWarnings("unused")
	private final static Logger logger = LoggerFactory.getLogger(TransactionMessageRestController.class);

	@Autowired
	private ITransactionMessageService transactionMessageService;

	/**
	 * 保存预发送消息
	 * 
	 * @param e
	 * @return 保存的消息id
	 */
	@PostMapping("saveMessageLocal")
	public String saveMessageLocal(@RequestBody TransactionMessageEntity e) {
		return transactionMessageService.saveMessageLocal(e);
	}

	/**
	 * 确认并发送消息
	 * 
	 * @param id
	 * @return 成功true | 失败false
	 */
	@GetMapping("confirmAndSendMessage")
	public boolean confirmAndSendMessage(@RequestParam("id") String id) {
		return transactionMessageService.confirmAndSendMessage(id);
	}

	/**
	 * 保存消息并直接发送
	 * 
	 * @param e
	 * @return 成功true | 失败false @
	 */
	@PostMapping("saveAndSendMessage")
	public boolean saveAndSendMessage(@RequestBody TransactionMessageEntity e) {
		return transactionMessageService.saveAndSendMessage(e);
	}

	/**
	 * 直接发送消息，不保存本地
	 * 
	 * @param e
	 * @return 成功true | 失败false @
	 */
	@PostMapping("directSendMessage")
	public boolean directSendMessage(@RequestBody TransactionMessageEntity e) {
		return transactionMessageService.directSendMessage(e);
	}

	/**
	 * 根据消息id重新发送消息,重新发送的消息,会将消息重发次数加1
	 * 
	 * @param e
	 * @return 成功true | 失败false @
	 */
	@GetMapping("reSendMessageById")
	public boolean reSendMessageById(@RequestParam("id") String id) {
		return transactionMessageService.reSendMessageById(id);
	}

	/**
	 * 根据id将消息标记为死亡状态
	 * 
	 * @param id
	 * @return @
	 */
	@GetMapping("setMessageToAreadlyDead")
	public boolean setMessageToAreadlyDead(@RequestParam("id") String id) {
		return transactionMessageService.setMessageToAreadlyDead(id);
	}

	/**
	 * 根据消息id获取消息,其中包括已经死亡的消息，不包括历史消息
	 * 
	 * @param id 消息的id
	 * @return 存在返回实体|不存在则返回null @
	 */
	@GetMapping("getMessageById")
	public TransactionMessageEntity getMessageById(@RequestParam("id") String id) {
		return transactionMessageService.getMessageById(id);
	}

	/**
	 * 根据消息id获取死亡的消息
	 * 
	 * @param id 消息的id
	 * @return 存在返回实体|不存在则返回null @
	 */
	@GetMapping("getDeathMessageById")
	public TransactionMessageEntity getDeathMessageById(@RequestParam("id") String id) {
		return transactionMessageService.getDeathMessageById(id) ; 
	}

	/**
	 * 根据消息id获取历史消息
	 * 
	 * @param id 消息的id
	 * @return 存在返回实体|不存在则返回null @
	 */
	@GetMapping("getHistoryMessageById")
	public TransactionMessageHistoryEntity getHistoryMessageById(@RequestParam("id") String id) {
		return transactionMessageService.getHistoryMessageById(id) ; 
	}

	/**
	 * 根据队名获取消息
	 * 
	 * @param String queueName 队列的id
	 * @return @
	 */
	@GetMapping("getMessageByTopic")
	public Page<TransactionMessageEntity> getMessageByTopic(
			@RequestParam("topic") String topic, 
			@RequestParam("pageNow") int pageNow, 
			@RequestParam("pageSize") int pageSize) {
		return transactionMessageService.getMessageByTopic(topic, pageNow, pageSize) ; 
	}

	/**
	 * 根据队列名和是否死亡获取消息
	 * 
	 * @param String queueName 队列的id , Map<String, Object> param
	 * @return @
	 */
	@GetMapping("getMessageByTopicAndParam")
	public Page<TransactionMessageEntity> getMessageByTopicAndParam(
			@RequestParam("topic") String topic, 
			@RequestParam("params") Map<String, Object> params, 
			@RequestParam("pageNow") int pageNow, 
			@RequestParam("pageSize") int pageSize) {
		return transactionMessageService.getMessageByTopicAndParam(topic, params, pageNow, pageSize) ; 
	}

	/**
	 * 根据消息的id删除消息,即移到历史表
	 * 
	 * @param id
	 * @return 成功true | 失败false @
	 */
	@GetMapping("deleteMessageById")
	public boolean deleteMessageById(@RequestParam("id") String id) {
		return transactionMessageService.deleteMessageById(id) ; 
	}

	/**
	 * 根据业务id确认消息已经成功消费,然后删除消息id
	 * 
	 * @param id
	 * @return @
	 */
	@GetMapping("confirmMessageByMessageId")
	public boolean confirmMessageByMessageId(@RequestParam("id") String id) {
		return transactionMessageService.confirmMessageByMessageId(id) ; 
	}

	/**
	 * 重发某个消息列队中的全部死亡消息
	 * 
	 * @param queueName 列队的名称
	 * @param batchSize 发送的条数
	 * @return 成功null | 失败返回失败消息的id @
	 */
	@GetMapping("reSendAllMessageByTopic")
	public List<String> reSendAllMessageByTopic(
			@RequestParam("topic") String topic, 
			@RequestParam("batchSize") int batchSize) {
		return transactionMessageService.reSendAllMessageByTopic(topic, batchSize) ; 
	}

	/**
	 * 分页获取消息数据
	 * 
	 * @param          <PageBean>
	 * @param params   参数
	 * @param pageNow  当前页
	 * @param pageSize 分页条数
	 * @return
	 */
	@GetMapping("listMessagePage")
	public Page<TransactionMessageEntity> listMessagePage(
			@RequestParam("pageNow") int pageNow, 
			@RequestParam("pageSize") int pageSize, 
			@RequestParam("params") Map<String, Object> params) {
		return transactionMessageService.listMessagePage(pageNow, pageSize, params) ; 
	}

	/**
	 * 发送kafka消息
	 * 
	 * @param messageBody
	 * @param topicName
	 * @return 成功则返回true，否则失败
	 */
	@GetMapping("sendMessage")
	boolean sendMessage(
			@RequestParam("messageBody") String messageBody, 
			@RequestParam("topic") String topic) {
		return transactionMessageService.sendMessage(messageBody, topic) ; 
	}

}
