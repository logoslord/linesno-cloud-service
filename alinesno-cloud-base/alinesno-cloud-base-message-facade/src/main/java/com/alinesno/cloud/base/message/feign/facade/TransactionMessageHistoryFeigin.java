package com.alinesno.cloud.base.message.feign.facade;

import org.springframework.cloud.openfeign.FeignClient;
import com.alinesno.cloud.common.facade.feign.IBaseFeign;
import com.alinesno.cloud.base.message.feign.dto.TransactionMessageHistoryDto;

/**
 * <p>  请求客户端 </p>
 *
 * @author LuoAnDong
 * @since 2018-12-16 18:25:27
 */
@FeignClient(name="alinesno-cloud-base-message" , path="transactionMessageHistory")
public interface TransactionMessageHistoryFeigin extends IBaseFeign<TransactionMessageHistoryDto> {

}
