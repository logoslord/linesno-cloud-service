package com.alinesno.cloud.base.wechat.entity;

import javax.persistence.Column;
import com.alinesno.cloud.common.core.orm.entity.BaseEntity;


/**
 * <p>
 * 
 * </p>
 *
 * @author LuoAnDong
 * @since 2018-12-16 18:11:54
 */
public class OrderEntity extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 发送的数据包
     */
	private String attach;
    /**
     * 随机字符串
     */
	@Column(name="nonce_str")
	private String nonceStr;
    /**
     * 返回的费用
     */
	@Column(name="notify_bill")
	private Double notifyBill;
    /**
     * 通知支付的时间
     */
	@Column(name="notify_time_end")
	private String notifyTimeEnd;
    /**
     * 下单的用户
     */
	@Column(name="open_id")
	private String openId;
    /**
     * 下单的费用
     */
	@Column(name="order_bill")
	private Double orderBill;
	@Column(name="order_id")
	private String orderId;
    /**
     * 订单的id
订单的id
     */
	@Column(name="out_trade_no")
	private String outTradeNo;
    /**
     * 备注信息
     */
	@Column(name="pay_remark")
	private String payRemark;
    /**
     * 签名
     */
	@Column(name="pay_sign")
	private String paySign;
    /**
     * 支付的状态(0下单成功|1支付成功|2支付失败)
     */
	@Column(name="pay_status")
	private String payStatus;
    /**
     * 预支付ID串，如: prepare_id=xxx
     */
	private String pkg;
    /**
     * 签名类型MD5
     */
	@Column(name="sign_type")
	private String signType;
    /**
     * 时间戳(s)
     */
	@Column(name="time_stamp")
	private String timeStamp;
    /**
     * 微信支付订单号
     */
	@Column(name="transaction_id")
	private String transactionId;


	public String getAttach() {
		return attach;
	}

	public void setAttach(String attach) {
		this.attach = attach;
	}

	public String getNonceStr() {
		return nonceStr;
	}

	public void setNonceStr(String nonceStr) {
		this.nonceStr = nonceStr;
	}

	public Double getNotifyBill() {
		return notifyBill;
	}

	public void setNotifyBill(Double notifyBill) {
		this.notifyBill = notifyBill;
	}

	public String getNotifyTimeEnd() {
		return notifyTimeEnd;
	}

	public void setNotifyTimeEnd(String notifyTimeEnd) {
		this.notifyTimeEnd = notifyTimeEnd;
	}

	public String getOpenId() {
		return openId;
	}

	public void setOpenId(String openId) {
		this.openId = openId;
	}

	public Double getOrderBill() {
		return orderBill;
	}

	public void setOrderBill(Double orderBill) {
		this.orderBill = orderBill;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getOutTradeNo() {
		return outTradeNo;
	}

	public void setOutTradeNo(String outTradeNo) {
		this.outTradeNo = outTradeNo;
	}

	public String getPayRemark() {
		return payRemark;
	}

	public void setPayRemark(String payRemark) {
		this.payRemark = payRemark;
	}

	public String getPaySign() {
		return paySign;
	}

	public void setPaySign(String paySign) {
		this.paySign = paySign;
	}

	public String getPayStatus() {
		return payStatus;
	}

	public void setPayStatus(String payStatus) {
		this.payStatus = payStatus;
	}

	public String getPkg() {
		return pkg;
	}

	public void setPkg(String pkg) {
		this.pkg = pkg;
	}

	public String getSignType() {
		return signType;
	}

	public void setSignType(String signType) {
		this.signType = signType;
	}

	public String getTimeStamp() {
		return timeStamp;
	}

	public void setTimeStamp(String timeStamp) {
		this.timeStamp = timeStamp;
	}

	public String getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}


	@Override
	public String toString() {
		return "OrderEntity{" +
			"attach=" + attach +
			", nonceStr=" + nonceStr +
			", notifyBill=" + notifyBill +
			", notifyTimeEnd=" + notifyTimeEnd +
			", openId=" + openId +
			", orderBill=" + orderBill +
			", orderId=" + orderId +
			", outTradeNo=" + outTradeNo +
			", payRemark=" + payRemark +
			", paySign=" + paySign +
			", payStatus=" + payStatus +
			", pkg=" + pkg +
			", signType=" + signType +
			", timeStamp=" + timeStamp +
			", transactionId=" + transactionId +
			"}";
	}
}
