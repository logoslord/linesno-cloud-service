package com.alinesno.cloud.base.wechat.service.impl;

import com.alinesno.cloud.base.wechat.entity.ErrorCodeEntity;
import com.alinesno.cloud.base.wechat.repository.ErrorCodeRepository;
import com.alinesno.cloud.base.wechat.service.IErrorCodeService;
import com.alinesno.cloud.common.core.services.impl.IBaseServiceImpl;
import org.springframework.stereotype.Service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * <p>  服务实现类 </p>
 *
 * @author LuoAnDong
 * @since 2018-12-16 18:11:54
 */
@Service
public class ErrorCodeServiceImpl extends IBaseServiceImpl<ErrorCodeRepository, ErrorCodeEntity, String> implements IErrorCodeService {

	//日志记录
	@SuppressWarnings("unused")
	private static final Logger log = LoggerFactory.getLogger(ErrorCodeServiceImpl.class);

}
