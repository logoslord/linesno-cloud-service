package com.alinesno.cloud.base.wechat.rest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.web.bind.annotation.RequestMapping;
import com.alinesno.cloud.base.wechat.entity.OauthAccessTokenEntity;
import com.alinesno.cloud.base.wechat.service.IOauthAccessTokenService;
import org.springframework.web.bind.annotation.RestController;
import com.alinesno.cloud.common.core.rest.BaseRestController;

/**
 * <p> 接口 </p>
 *
 * @author LuoAnDong
 * @since 2018-12-16 18:11:54
 */
@Scope("prototype")
@RestController
@RequestMapping("oauthAccessToken")
public class OauthAccessTokenRestController extends BaseRestController<OauthAccessTokenEntity , IOauthAccessTokenService> {

	//日志记录
	@SuppressWarnings("unused")
	private final static Logger logger = LoggerFactory.getLogger(OauthAccessTokenRestController.class);

}
