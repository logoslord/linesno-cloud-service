package com.alinesno.cloud.base.wechat;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.scheduling.annotation.EnableAsync;

import com.ctrip.framework.apollo.spring.annotation.EnableApolloConfig;

import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * 启动入口
 * 
 * @author LuoAnDong 
 * @since 2018-12-16 18:12:368
 */
@EnableApolloConfig //阿波罗分布式配置
@SpringBootApplication
@EnableAsync // 开启异步任务
@EnableSwagger2 //开启swagger2 
@EnableEurekaClient  // 开启eureka
public class AlinesnoApplication {

	public static void main(String[] args) {
		SpringApplication.run(AlinesnoApplication.class, args);
	}

}
