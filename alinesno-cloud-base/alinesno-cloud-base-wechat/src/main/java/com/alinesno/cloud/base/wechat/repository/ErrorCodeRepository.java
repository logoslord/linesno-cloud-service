package com.alinesno.cloud.base.wechat.repository;

import com.alinesno.cloud.base.wechat.entity.ErrorCodeEntity;
import com.alinesno.cloud.common.core.orm.repository.IBaseJpaRepository;

/**
 * <p>
  *  持久层接口
 * </p>
 *
 * @author LuoAnDong
 * @since 2018-12-16 18:11:54
 */
public interface ErrorCodeRepository extends IBaseJpaRepository<ErrorCodeEntity, String> {

}
