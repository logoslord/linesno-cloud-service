package com.alinesno.cloud.base.wechat.service.impl;

import com.alinesno.cloud.base.wechat.entity.OrderEntity;
import com.alinesno.cloud.base.wechat.repository.OrderRepository;
import com.alinesno.cloud.base.wechat.service.IOrderService;
import com.alinesno.cloud.common.core.services.impl.IBaseServiceImpl;
import org.springframework.stereotype.Service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * <p>  服务实现类 </p>
 *
 * @author LuoAnDong
 * @since 2018-12-16 18:11:54
 */
@Service
public class OrderServiceImpl extends IBaseServiceImpl<OrderRepository, OrderEntity, String> implements IOrderService {

	//日志记录
	@SuppressWarnings("unused")
	private static final Logger log = LoggerFactory.getLogger(OrderServiceImpl.class);

}
