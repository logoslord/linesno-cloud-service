package com.alinesno.cloud.base.wechat.service.impl;

import com.alinesno.cloud.base.wechat.entity.MenuActionEntity;
import com.alinesno.cloud.base.wechat.repository.MenuActionRepository;
import com.alinesno.cloud.base.wechat.service.IMenuActionService;
import com.alinesno.cloud.common.core.services.impl.IBaseServiceImpl;
import org.springframework.stereotype.Service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * <p>  服务实现类 </p>
 *
 * @author LuoAnDong
 * @since 2018-12-16 18:11:54
 */
@Service
public class MenuActionServiceImpl extends IBaseServiceImpl<MenuActionRepository, MenuActionEntity, String> implements IMenuActionService {

	//日志记录
	@SuppressWarnings("unused")
	private static final Logger log = LoggerFactory.getLogger(MenuActionServiceImpl.class);

}
