package com.alinesno.cloud.base.wechat.entity;

import javax.persistence.Column;
import javax.persistence.Table;
import javax.persistence.Entity;
import com.alinesno.cloud.common.core.orm.entity.BaseEntity;


/**
 * <p>
 * 
 * </p>
 *
 * @author LuoAnDong
 * @since 2018-12-16 18:11:54
 */
@Entity
@Table(name="error_record")
public class ErrorRecordEntity extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 错误的 token
     */
	@Column(name="access_token")
	private String accessToken;
    /**
     * 错误的时间
     */
	@Column(name="err_time")
	private String errTime;
    /**
     * 错误的代码
     */
	private String errcode;
    /**
     * 错误的信息
     */
	@Column(name="error_msg")
	private String errorMsg;
    /**
     * 错误说明
     */
	@Column(name="error_text")
	private String errorText;
	@Column(name="open_id")
	private String openId;


	public String getAccessToken() {
		return accessToken;
	}

	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}

	public String getErrTime() {
		return errTime;
	}

	public void setErrTime(String errTime) {
		this.errTime = errTime;
	}

	public String getErrcode() {
		return errcode;
	}

	public void setErrcode(String errcode) {
		this.errcode = errcode;
	}

	public String getErrorMsg() {
		return errorMsg;
	}

	public void setErrorMsg(String errorMsg) {
		this.errorMsg = errorMsg;
	}

	public String getErrorText() {
		return errorText;
	}

	public void setErrorText(String errorText) {
		this.errorText = errorText;
	}

	public String getOpenId() {
		return openId;
	}

	public void setOpenId(String openId) {
		this.openId = openId;
	}


	@Override
	public String toString() {
		return "ErrorRecordEntity{" +
			"accessToken=" + accessToken +
			", errTime=" + errTime +
			", errcode=" + errcode +
			", errorMsg=" + errorMsg +
			", errorText=" + errorText +
			", openId=" + openId +
			"}";
	}
}
