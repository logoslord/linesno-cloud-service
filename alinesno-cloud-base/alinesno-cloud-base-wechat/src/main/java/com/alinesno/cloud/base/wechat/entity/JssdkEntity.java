package com.alinesno.cloud.base.wechat.entity;

import javax.persistence.Column;
import com.alinesno.cloud.common.core.orm.entity.BaseEntity;


/**
 * <p>
 * 
 * </p>
 *
 * @author LuoAnDong
 * @since 2018-12-16 18:11:54
 */
public class JssdkEntity extends BaseEntity {

    private static final long serialVersionUID = 1L;

	private Integer errcode;
	private String errmsg;
	@Column(name="expires_in")
	private Integer expiresIn;
	@Column(name="jssdk_id")
	private String jssdkId;
	private String ticket;


	public Integer getErrcode() {
		return errcode;
	}

	public void setErrcode(Integer errcode) {
		this.errcode = errcode;
	}

	public String getErrmsg() {
		return errmsg;
	}

	public void setErrmsg(String errmsg) {
		this.errmsg = errmsg;
	}

	public Integer getExpiresIn() {
		return expiresIn;
	}

	public void setExpiresIn(Integer expiresIn) {
		this.expiresIn = expiresIn;
	}

	public String getJssdkId() {
		return jssdkId;
	}

	public void setJssdkId(String jssdkId) {
		this.jssdkId = jssdkId;
	}

	public String getTicket() {
		return ticket;
	}

	public void setTicket(String ticket) {
		this.ticket = ticket;
	}


	@Override
	public String toString() {
		return "JssdkEntity{" +
			"errcode=" + errcode +
			", errmsg=" + errmsg +
			", expiresIn=" + expiresIn +
			", jssdkId=" + jssdkId +
			", ticket=" + ticket +
			"}";
	}
}
