package com.alinesno.cloud.base.wechat.entity;

import javax.persistence.Column;
import javax.persistence.Table;
import javax.persistence.Entity;
import com.alinesno.cloud.common.core.orm.entity.BaseEntity;


/**
 * <p>
 * 
 * </p>
 *
 * @author LuoAnDong
 * @since 2018-12-16 18:11:54
 */
@Entity
@Table(name="oauth_access_token")
public class OauthAccessTokenEntity extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 网页授权接口调用凭证,注意：此access_token与基础支持的access_token不同
     */
	@Column(name="access_token")
	private String accessToken;
	private String errcode;
	private String errmsg;
    /**
     * access_token接口调用凭证超时时间，单位（秒）
     */
	@Column(name="expires_in")
	private String expiresIn;
    /**
     * 用户唯一标识
     */
	private String openid;
    /**
     * 用户刷新access_token
     */
	@Column(name="refresh_token")
	private String refreshToken;
    /**
     * 用户授权的作用域，使用逗号（,）分隔
     */
	private String scope;
	@Column(name="token_id")
	private String tokenId;


	public String getAccessToken() {
		return accessToken;
	}

	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}

	public String getErrcode() {
		return errcode;
	}

	public void setErrcode(String errcode) {
		this.errcode = errcode;
	}

	public String getErrmsg() {
		return errmsg;
	}

	public void setErrmsg(String errmsg) {
		this.errmsg = errmsg;
	}

	public String getExpiresIn() {
		return expiresIn;
	}

	public void setExpiresIn(String expiresIn) {
		this.expiresIn = expiresIn;
	}

	public String getOpenid() {
		return openid;
	}

	public void setOpenid(String openid) {
		this.openid = openid;
	}

	public String getRefreshToken() {
		return refreshToken;
	}

	public void setRefreshToken(String refreshToken) {
		this.refreshToken = refreshToken;
	}

	public String getScope() {
		return scope;
	}

	public void setScope(String scope) {
		this.scope = scope;
	}

	public String getTokenId() {
		return tokenId;
	}

	public void setTokenId(String tokenId) {
		this.tokenId = tokenId;
	}


	@Override
	public String toString() {
		return "OauthAccessTokenEntity{" +
			"accessToken=" + accessToken +
			", errcode=" + errcode +
			", errmsg=" + errmsg +
			", expiresIn=" + expiresIn +
			", openid=" + openid +
			", refreshToken=" + refreshToken +
			", scope=" + scope +
			", tokenId=" + tokenId +
			"}";
	}
}
