package com.alinesno.cloud.base.wechat.feign.dto;

import com.alinesno.cloud.common.facade.feign.BaseDto;

/**
 * <p> 传输对象</p>
 *
 * @author LuoAnDong
 * @since 2018-12-16 18:11:54
 */
@SuppressWarnings("serial")
public class OauthAccessTokenDto extends BaseDto {

    /**
     * 网页授权接口调用凭证,注意：此access_token与基础支持的access_token不同
     */
	private String accessToken;
	
	private String errcode;
	
	private String errmsg;
	
    /**
     * access_token接口调用凭证超时时间，单位（秒）
     */
	private String expiresIn;
	
    /**
     * 用户唯一标识
     */
	private String openid;
	
    /**
     * 用户刷新access_token
     */
	private String refreshToken;
	
    /**
     * 用户授权的作用域，使用逗号（,）分隔
     */
	private String scope;
	
	private String tokenId;
	


	public String getAccessToken() {
		return accessToken;
	}

	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}

	public String getErrcode() {
		return errcode;
	}

	public void setErrcode(String errcode) {
		this.errcode = errcode;
	}

	public String getErrmsg() {
		return errmsg;
	}

	public void setErrmsg(String errmsg) {
		this.errmsg = errmsg;
	}

	public String getExpiresIn() {
		return expiresIn;
	}

	public void setExpiresIn(String expiresIn) {
		this.expiresIn = expiresIn;
	}

	public String getOpenid() {
		return openid;
	}

	public void setOpenid(String openid) {
		this.openid = openid;
	}

	public String getRefreshToken() {
		return refreshToken;
	}

	public void setRefreshToken(String refreshToken) {
		this.refreshToken = refreshToken;
	}

	public String getScope() {
		return scope;
	}

	public void setScope(String scope) {
		this.scope = scope;
	}

	public String getTokenId() {
		return tokenId;
	}

	public void setTokenId(String tokenId) {
		this.tokenId = tokenId;
	}

}
