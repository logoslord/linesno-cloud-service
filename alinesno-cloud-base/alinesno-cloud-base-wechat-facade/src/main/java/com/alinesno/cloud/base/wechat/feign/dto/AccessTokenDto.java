package com.alinesno.cloud.base.wechat.feign.dto;

import com.alinesno.cloud.common.facade.feign.BaseDto;

/**
 * <p> 传输对象</p>
 *
 * @author LuoAnDong
 * @since 2018-12-16 18:11:54
 */
@SuppressWarnings("serial")
public class AccessTokenDto extends BaseDto {

    /**
     * 微信accessToken
     */
	private String accessToken;
	
    /**
     * 错误代码
     */
	private String errcode;
	
    /**
     * 错误信息
     */
	private String errmsg;
	
    /**
     * token超时时间
     */
	private String expiresIn;
	
    /**
     * token唯一id号
     */
	private String tokenId;
	


	public String getAccessToken() {
		return accessToken;
	}

	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}

	public String getErrcode() {
		return errcode;
	}

	public void setErrcode(String errcode) {
		this.errcode = errcode;
	}

	public String getErrmsg() {
		return errmsg;
	}

	public void setErrmsg(String errmsg) {
		this.errmsg = errmsg;
	}

	public String getExpiresIn() {
		return expiresIn;
	}

	public void setExpiresIn(String expiresIn) {
		this.expiresIn = expiresIn;
	}

	public String getTokenId() {
		return tokenId;
	}

	public void setTokenId(String tokenId) {
		this.tokenId = tokenId;
	}

}
