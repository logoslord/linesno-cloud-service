package com.alinesno.cloud.base.wechat.feign.dto;

import com.alinesno.cloud.common.facade.feign.BaseDto;

/**
 * <p> 传输对象</p>
 *
 * @author LuoAnDong
 * @since 2018-12-16 18:11:54
 */
@SuppressWarnings("serial")
public class PayNotifyDto extends BaseDto {

    /**
     * 微信分配的公众账号ID（企业号corpid即为此appId）
     */
	private String appid;
	
    /**
     * 商家数据包，原样返回
     */
	private String attach;
	
    /**
     * 银行类型，采用字符串类型的银行标识，银行类型见银行列表
     */
	private String bankType;
	
    /**
     * 现金支付金额订单现金支付金额，详见支付金额
     */
	private Double cashFee;
	
    /**
     * 货币类型，符合ISO4217标准的三位字母代码，默认人民币：CNY，其他值列表详见货币类型
     */
	private String cashFeeType;
	
    /**
     * 代金券使用数量
     */
	private Integer couponCount;
	
    /**
     * 代金券金额<=订单金额，订单金额-代金券金额=现金支付金额，详见支付金额
     */
	private Double couponFee;
	
    /**
     * 微信支付分配的终端设备号，
     */
	private String deviceInfo;
	
    /**
     * 错误返回的信息描述
     */
	private String errCodDes;
	
    /**
     * 错误返回的信息描述
     */
	private String errCode;
	
    /**
     * 货币类型，符合ISO4217标准的三位字母代码，默认人民币：CNY，其他值列表详见货币类型
     */
	private String feeType;
	
    /**
     * 否关注公众账号，Y-关注，N-未关注，仅在公众账号类型支付有效
     */
	private String isSubscribe;
	
    /**
     * 微信支付分配的商户号
     */
	private String mchId;
	
    /**
     * 随机字符串，不长于32位
     */
	private String nonceStr;
	
    /**
     * 通用的id
     */
	private String notifyId;
	
    /**
     * 用户在商户appid下的唯一标识
     */
	private String openid;
	
    /**
     * 商户系统的订单号，与请求一致。
     */
	private String outTradeNo;
	
    /**
     * SUCCESS/FAIL
     */
	private String resultCode;
	
    /**
     * 应结订单金额=订单金额-非充值代金券金额，应结订单金额<=订单金额。
     */
	private Double settlementTotalFee;
	
    /**
     * 签名，详见签名算法
     */
	private String sign;
	
    /**
     * HMAC-SHA256 签名类型，目前支持HMAC-SHA256和MD5，默认为MD5
     */
	private String signType;
	
    /**
     * 支付完成时间，格式为yyyyMMddHHmmss，如2009年12月25日9点10分10秒表示为20091225091010。其他详见时间规则
     */
	private String timeEnd;
	
    /**
     * 订单总金额，单位为分
     */
	private Double totalFee;
	
    /**
     * JSAPI、NATIVE、APP
     */
	private String tradeType;
	
    /**
     * 微信支付订单号
     */
	private String transactionId;
	


	public String getAppid() {
		return appid;
	}

	public void setAppid(String appid) {
		this.appid = appid;
	}

	public String getAttach() {
		return attach;
	}

	public void setAttach(String attach) {
		this.attach = attach;
	}

	public String getBankType() {
		return bankType;
	}

	public void setBankType(String bankType) {
		this.bankType = bankType;
	}

	public Double getCashFee() {
		return cashFee;
	}

	public void setCashFee(Double cashFee) {
		this.cashFee = cashFee;
	}

	public String getCashFeeType() {
		return cashFeeType;
	}

	public void setCashFeeType(String cashFeeType) {
		this.cashFeeType = cashFeeType;
	}

	public Integer getCouponCount() {
		return couponCount;
	}

	public void setCouponCount(Integer couponCount) {
		this.couponCount = couponCount;
	}

	public Double getCouponFee() {
		return couponFee;
	}

	public void setCouponFee(Double couponFee) {
		this.couponFee = couponFee;
	}

	public String getDeviceInfo() {
		return deviceInfo;
	}

	public void setDeviceInfo(String deviceInfo) {
		this.deviceInfo = deviceInfo;
	}

	public String getErrCodDes() {
		return errCodDes;
	}

	public void setErrCodDes(String errCodDes) {
		this.errCodDes = errCodDes;
	}

	public String getErrCode() {
		return errCode;
	}

	public void setErrCode(String errCode) {
		this.errCode = errCode;
	}

	public String getFeeType() {
		return feeType;
	}

	public void setFeeType(String feeType) {
		this.feeType = feeType;
	}

	public String getIsSubscribe() {
		return isSubscribe;
	}

	public void setIsSubscribe(String isSubscribe) {
		this.isSubscribe = isSubscribe;
	}

	public String getMchId() {
		return mchId;
	}

	public void setMchId(String mchId) {
		this.mchId = mchId;
	}

	public String getNonceStr() {
		return nonceStr;
	}

	public void setNonceStr(String nonceStr) {
		this.nonceStr = nonceStr;
	}

	public String getNotifyId() {
		return notifyId;
	}

	public void setNotifyId(String notifyId) {
		this.notifyId = notifyId;
	}

	public String getOpenid() {
		return openid;
	}

	public void setOpenid(String openid) {
		this.openid = openid;
	}

	public String getOutTradeNo() {
		return outTradeNo;
	}

	public void setOutTradeNo(String outTradeNo) {
		this.outTradeNo = outTradeNo;
	}

	public String getResultCode() {
		return resultCode;
	}

	public void setResultCode(String resultCode) {
		this.resultCode = resultCode;
	}

	public Double getSettlementTotalFee() {
		return settlementTotalFee;
	}

	public void setSettlementTotalFee(Double settlementTotalFee) {
		this.settlementTotalFee = settlementTotalFee;
	}

	public String getSign() {
		return sign;
	}

	public void setSign(String sign) {
		this.sign = sign;
	}

	public String getSignType() {
		return signType;
	}

	public void setSignType(String signType) {
		this.signType = signType;
	}

	public String getTimeEnd() {
		return timeEnd;
	}

	public void setTimeEnd(String timeEnd) {
		this.timeEnd = timeEnd;
	}

	public Double getTotalFee() {
		return totalFee;
	}

	public void setTotalFee(Double totalFee) {
		this.totalFee = totalFee;
	}

	public String getTradeType() {
		return tradeType;
	}

	public void setTradeType(String tradeType) {
		this.tradeType = tradeType;
	}

	public String getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}

}
