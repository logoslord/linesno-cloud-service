package com.alinesno.cloud.base.boot.web;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ImportResource;
import org.springframework.scheduling.annotation.EnableAsync;

/**
 * 启动入口
 * @EnableSwagger2 // 开启swagger2
 * @author LuoAnDong
 * @since 2018年8月7日 上午8:45:02
 */
@SpringBootApplication(exclude = DataSourceAutoConfiguration.class) // 去掉datasources配置
@EnableAsync // 开启异步任务
@EnableEurekaClient // 开启eureka
@ComponentScan(basePackages={"com.alinesno.cloud.common.web", "com.alinesno.cloud.base.boot.web"}) // 扫描公共web包
@ImportResource("classpath:spring/spring-context.xml") // 自定义xml文件(必须)
@EnableFeignClients(basePackages="com.alinesno.cloud.**.feign.facade.**") //扫描feign包下的，变成接口可调用包
public class AlinesnoApplication {
	
	public static void main(String[] args) {
		SpringApplication.run(AlinesnoApplication.class, args);
	}

}
