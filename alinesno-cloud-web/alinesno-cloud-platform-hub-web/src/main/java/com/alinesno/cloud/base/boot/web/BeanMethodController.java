package com.alinesno.cloud.base.boot.web;

import com.alinesno.cloud.common.facade.feign.BaseDto;
import com.alinesno.cloud.common.facade.feign.IBaseFeign;
import com.alinesno.cloud.common.web.base.controller.BaseController;

/**
 * 基础操作层
 * @author LuoAnDong
 * @since 2018年12月23日 下午12:21:42
 */
public abstract class BeanMethodController<DTO extends BaseDto , FEIGN extends IBaseFeign<DTO>> extends BaseController {

	
	
}
