package com.alinesno.cloud.base.boot.web.thymeleaf;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class WorkImport {

    @Bean
    public CustomDialect testDialect(){
        return new CustomDialect();
    }
    
    @Bean
    public TagDialect tagDialect(){
        return new TagDialect();
    }
}