package com.alinesno.cloud.base.boot.web.module.settings;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alinesno.cloud.base.boot.feign.dto.ManagerSourceGenerateDto;
import com.alinesno.cloud.base.boot.feign.facade.ManagerSourceGenerateFeigin;
import com.alinesno.cloud.base.boot.web.module.BeanMethodController;
import com.alinesno.cloud.common.core.constants.SpringInstanceScope;
import com.alinesno.cloud.common.web.base.advice.TranslateCode;
import com.alinesno.cloud.common.web.base.bean.DatatablesPageBean;
import com.alinesno.cloud.common.web.base.form.FormToken;
import com.alinesno.cloud.common.web.base.response.ResponseBean;
import com.alinesno.cloud.common.web.base.response.ResponseGenerator;
import com.alinesno.cloud.common.web.login.aop.AccountRecord;
import com.alinesno.cloud.common.web.login.aop.AccountRecord.RecordType;

/**
 * 代码生成器
 * 
 * @author LuoAnDong
 * @since 2019年4月8日 下午10:29:02
 */
@Controller
@Scope(SpringInstanceScope.PROTOTYPE)
@RequestMapping("boot/platform/source")
public class SourceGenerateController extends BeanMethodController<ManagerSourceGenerateDto, ManagerSourceGenerateFeigin> {

	private static final Logger log = LoggerFactory.getLogger(SourceGenerateController.class);

	@TranslateCode(value = "[{hasStatus:has_status}]")
	@ResponseBody
	@RequestMapping("/datatables")
	public DatatablesPageBean datatables(HttpServletRequest request, Model model, DatatablesPageBean page) {
		log.debug("page = {}", ToStringBuilder.reflectionToString(page));
		return this.toPage(model, feign, page);
	}
	
	/**
	 * 保存新对象 
	 * @param model
	 * @param managerCodeDto
	 * @return
	 */
	@AccountRecord(type=RecordType.OPERATE_SAVE)
	@FormToken(remove=true)
	@ResponseBody
	@PostMapping("/generator")
	public ResponseBean generator(Model model , HttpServletRequest request, ManagerSourceGenerateDto dto) {
		dto = feign.save(dto) ; 
		String downpath = "" ;  // 生成下载路径
		
		return ResponseGenerator.ok(downpath) ; 	
	}
	
}
