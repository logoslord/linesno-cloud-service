package com.alinesno.cloud.base.boot.web.module.dashboard;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.alinesno.cloud.common.core.constants.SpringInstanceScope;
import com.alinesno.cloud.common.web.base.controller.BaseController;

/**
 * 控制层
 * @author LuoAnDong
 * @since 2018年11月27日 上午6:41:40
 */
@Controller
@RequestMapping("boot/platform/copyright")
@Scope(SpringInstanceScope.PROTOTYPE)
public class CopyrightController extends BaseController {

	private static final Logger log = LoggerFactory.getLogger(CopyrightController.class) ; 

	/**
	 * 关于内容
	 * @return
	 */
	@RequestMapping("/about")
    public void dashboard(){
		log.debug("about");
    }
	
}
