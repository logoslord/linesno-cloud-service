package com.alinesno.cloud.base.boot.web.module.platform;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alinesno.cloud.base.boot.feign.dto.ManagerResourceDto;
import com.alinesno.cloud.base.boot.feign.facade.ManagerResourceFeigin;
import com.alinesno.cloud.base.boot.web.module.BeanMethodController;
import com.alinesno.cloud.common.core.constants.SpringInstanceScope;
import com.alinesno.cloud.common.web.base.advice.TranslateCode;
import com.alinesno.cloud.common.web.base.bean.DatatablesPageBean;
import com.alinesno.cloud.common.web.base.form.FormToken;
import com.alinesno.cloud.common.web.base.response.ResponseBean;
import com.alinesno.cloud.common.web.base.response.ResponseGenerator;

/**
 * 菜单管理 
 * @author LuoAnDong
 * @since 2018年12月7日 下午10:58:20
 */
@Controller
@Scope(SpringInstanceScope.PROTOTYPE)
@RequestMapping("boot/platform/menus")
public class MenusController extends BeanMethodController<ManagerResourceDto, ManagerResourceFeigin> {

	private static final Logger log = LoggerFactory.getLogger(MenusController.class) ; 

	@Autowired
	private ManagerResourceFeigin managerResourceFeigin ; 
	
	@TranslateCode("[{hasStatus:has_status,menuType:menus_type}]")
	@ResponseBody
	@PostMapping("/datatables")
    public DatatablesPageBean datatables(HttpServletRequest request , Model model ,  DatatablesPageBean page){
		log.debug("page = {}" , ToStringBuilder.reflectionToString(page));
		return this.toPage(model, managerResourceFeigin , page) ;
    }
	
	/**
	 * 菜单管理查询功能  
	 * @return
	 */
	@FormToken(save=true)
	@GetMapping("/modify")
    public void modify(Model model , HttpServletRequest request , String id){
		ManagerResourceDto oldBean = managerResourceFeigin.getOne(id) ; 
		model.addAttribute("bean", oldBean) ; 
    }

	/**
	 * 显示所有菜单数据
	 * @param model
	 * @param id
	 * @return
	 */
	@ResponseBody
	@PostMapping("/menusData")
    public List<ManagerResourceDto> menusData(Model model , String id){
		
		log.debug("application id = {}" , id);
		model.addAttribute("applicationId", id) ; 
		
		return managerResourceFeigin.findAll()  ;
    }

	/**
	 * 显示所有父级菜单
	 * @param model
	 * @param id
	 */
	@GetMapping("/parents")
    public void menus(Model model , String id){
		log.debug("application id = {}" , id);
		model.addAttribute("applicationId", id) ; 
    }

	/**
	 * 更新父类
	 * @param model
	 * @param request
	 * @param id
	 */
	@ResponseBody
	@GetMapping("/updateResourceParent")
    public ResponseBean updateResourceParent(Model model , HttpServletRequest request , String id , String resourceParent){
		ManagerResourceDto e = managerResourceFeigin.getOne(id) ; 
		
		e.setResourceParent(resourceParent); 
		managerResourceFeigin.save(e) ; 
		
		return ResponseGenerator.ok("") ; 
    }

}
