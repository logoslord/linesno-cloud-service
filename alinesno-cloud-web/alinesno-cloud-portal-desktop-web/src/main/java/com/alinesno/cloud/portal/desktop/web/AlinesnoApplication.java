package com.alinesno.cloud.portal.desktop.web;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

import com.alinesno.cloud.common.web.enable.EnableAlinesnoCommonWeb;

/**
 * 启动入口
 * @author LuoAnDong
 * @EnableEurekaClient // 开启eureka
 * @since 2018年8月7日 上午8:45:02
 */
@SpringBootApplication(exclude = DataSourceAutoConfiguration.class) // 去掉datasources配置
@EnableAlinesnoCommonWeb
public class AlinesnoApplication {
	
	public static void main(String[] args) {
		SpringApplication.run(AlinesnoApplication.class, args);
	}

}
