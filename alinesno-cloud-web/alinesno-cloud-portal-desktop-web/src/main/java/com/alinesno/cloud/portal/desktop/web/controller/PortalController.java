package com.alinesno.cloud.portal.desktop.web.controller;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.alinesno.cloud.common.core.constants.SpringInstanceScope;
import com.alinesno.cloud.common.web.base.controller.BaseController;

/**
 * 控制层
 * @author LuoAnDong
 * @since 2018年11月27日 上午6:41:40
 */
@Controller
@Scope(SpringInstanceScope.PROTOTYPE)
public class PortalController extends BaseController {

	private static final Logger log = LoggerFactory.getLogger(PortalController.class) ; 
	
	@RequestMapping(value = "/")
	public String index(Model model , HttpServletRequest request) {
		log.debug("进入首页");
		return "portal/deskweb/index" ; 
	}

	
}
