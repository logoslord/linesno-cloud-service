# 工程服务和规划列表

## 目录规划
* [目录规划](README.md)
    * [组件功能规划](00_learn/17_组件功能列表.md)
    * [默认功能](00_learn/02_默认功能.md)

## 后台教程
* [管理平台](README.md)
    * [应用管理](README.md)
    * [单点接入](00_learn/04_单点接入.md)
* [使用教程](README.md)
    * [代码生成器](00_learn/05_代码生成器.md)
    * [服务教程](README.md)
    * [组件教程](README.md)
    * [前端教程](README.md)
    * [发布教程](README.md)
* [开发教程](README.md)
    * [获取当前会话](README.md)
    * [主键生成策略](README.md)
    * [服务调用](README.md)
    * [异常处理](README.md)
    * [日志处理](README.md)
    * [缓存使用](README.md)
    * [消息使用](README.md)
    * [验证码开启](README.md)
    * [日志埋点](README.md)
    * [多数据库源](README.md)

## 后台功能
* [功能](00_learn/18_防重复提交.md)
    * [防重复提交](00_learn/18_防重复提交.md)
    * [权限管理](README.md)
* [分布式](00_learn/15_分布式事务.md)
    * [定时任务](00_learn/15_分布式事务.md)
    * [分布式事务](00_learn/15_分布式事务.md)
    * [分布式锁](00_learn/16_分布式锁.md)
    * [分布式限流](00_learn/19_分布式限流.md)
